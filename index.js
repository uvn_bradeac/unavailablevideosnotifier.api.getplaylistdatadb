const AWS = require('aws-sdk')
const dynamodb = new AWS.DynamoDB({apiVersion: '2012-08-10'})

exports.handler = (event, context, callback) => {
    let channelId = event.query.channelid
    let playlistIds = []

    let getData = (channelId) => {
        var params = {
            Key: {
                "channelId": {
                    S: channelId
                }
            }, 
            TableName: "playlists"
        }

        dynamodb.getItem(params, (err, data) => {
            if (err) {
                console.log('Error. Getting item(s) from dynamodb failed: ', JSON.stringify(err, null, 2))
                callback(err, null)
            }
            else {
                try {
                    playlistIds = JSON.parse(data.Item.playlistIds.S)
                    callback(null, playlistIds)
                }
                catch(e) {
                    if (data.Item.playlistIds.S.length > 0) {
                        playlistIds = data.Item.playlistIds.S
                        callback(null, playlistIds)
                    }
                    callback(e, null)
                }
            }
        })
    }

    getData(channelId)
}